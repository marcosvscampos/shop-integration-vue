const PageNotFound = {
    template: '<h1> 404: Page Not Found </h1>'
}

const Home = {
    template : '#homepage'
};

const About = {
    name: 'About',
    template : '#about'
};

const AboutContact = {
    //Nomear cada component ajuda a lidar com erros dentro do próprio component
    //no console do browser 
    name: 'AboutContact',
    template : `
        <div>
            <h2> This is some contact information about me </h2>
            <p>Find me online, in person or on the phone</p>
        </div>`
};

const AboutFood = {
    name: 'AboutFood',
    template : `
        <div>
            <h2>Food</h2>
            <p>I really like chocolate, sweets and apples</p>
        </div>    
    `
}

const MeetTheTeam = {
    template : '#meettheteam'
};

const User = {
    template : '<h1> {{ formattedName }} is {{ formattedEmotion }} </h1>',
    props: {
        name: String,
        emotion: {
            type: String,
            default: 'happy'
        }
    },
    computed: {
        formattedName(){
            return this.name.charAt(0).toUpperCase() + this.name.slice(1);
        }, 
        formattedEmotion(){
           return this.emotion.charAt(0).toUpperCase() + this.emotion.slice(1); 
        }
    }
}

const router = new VueRouter({
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            // O path '*' valida qualquer URL desconhecida das que foram definidas no VueRouter
            // e faz o redirecionamento para a página especificada (PageNotFound)
            path: '*',
            component : PageNotFound
        },
        {
            path: '/about',
            // Especifica onde cada component vai ser colocado de acordo com os nomes das <router-view>
            // os components que forem colocados na key default seram renderizados na <router-view> sem nome
            components: {
                default: About,
                sidebar: AboutContact
            },
            //Usa-se o children para a criação de sub-urls para acesso mais rápido
            //a sessões dentro de outras sessões principais 
            children: [
                {
                    name: 'food',
                    path: 'food',
                    component: AboutFood
                }
            ]
        },
        {
            path: '/about/meet-the-team',
            component: MeetTheTeam
        },
        {
            path: '/:name/user',
            component: User,
            props: true
        },
        {
            //Nomear uma rota faz com que seja mais facil atribuí-la em um <router-link>
            //não sendo necessário colocar a URI no atributo "to" e sim passar o objeto com 
            //o nome e os atributos -> Ex: :to="{name: 'user', params: {name: 'sarah', emotion: 'happy'}}
            name: 'user',
            path: '/:name/user/:emotion',
            component: User,
            // Com o props=true é possivel carregar os valores que vem do path
            //direto no props do component para serem manipulados com mais facilidade
            props: true
        }
    ],
    //Esse atributos fazem com que as classes CSS aplicadas nos components, 
    //quando ativos, tenham um nome mais fácil de ler
    linkActiveClass: 'active',
    linkExactActiveClass: 'current'
});

const app = new Vue({
    el: "#app",
    router,
    mounted(){
        console.log(this.$route);
    }
});